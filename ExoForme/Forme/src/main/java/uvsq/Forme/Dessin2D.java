package uvsq.Forme;

import java.io.Serializable;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;

public class Dessin2D implements Serializable {
	Map<String,List> Formes;
	public Dessin2D() {
		Formes= new LinkedHashMap<>();
	}

	public void ajouter(Forme f)
	{
		Formes.put(f.nom,f.points);
	}
	
	public void afficher()
	{
		for (java.util.Map.Entry<String,List> elt : Formes.entrySet()) {
			System.out.println("forme: "+elt.getKey());
			List<Point> l= elt.getValue();
			ListIterator<Point> it = l.listIterator();
		      while(it.hasNext()){
		    	  Point p = it.next();
		    	  System.out.println(p.Afficher());
		}
		}
	}
	
	public void deplacer(int xx,int yy) {
		for (java.util.Map.Entry<String,List> elt : Formes.entrySet()) {
			List<Point> l= elt.getValue();
			ListIterator<Point> it = l.listIterator();
		      while(it.hasNext()){
		    	  Point p = it.next();
		    	  p.setX(p.getX()+xx);
		  		  p.sety(p.getY()+yy);    	  
		}
		}
	}
	
	public void sauvegarder()
	{
		ObjectOutputStream oos;
		try{
			oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("fic.txt"))));
			oos.writeObject(this);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();}
	}
	
	
	public void charger()
	{
		ObjectInputStream ois;
		try{
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("fic.txt"))));
			try {
				((Dessin2D)ois.readObject()).afficher();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();}
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();}
	}
}
