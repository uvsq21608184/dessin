package uvsq.Forme;

import java.io.Serializable;
import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
public class Forme implements Serializable  {
	
	public String nom;
	public List<Point> points=new ArrayList<Point>();;
	
	public Forme(String form){
		this.nom = form;
		Point p=new Point();
		points.add(p);
	}
	public Forme(String form,Cercle p){
		this.nom = form;
		points.add(p.centre);
	}
	public Forme(String form,Rectangle r){
		this.nom = form;
		points.add(r.BG);
		points.add(r.HG);
		points.add(r.BD);
		points.add(r.HD);	
	}

	public String afficher() {
		return nom+"  ";
	}
	
	public void deplacer(int xx,int yy) {
	    ListIterator<Point> it = points.listIterator();
	      while(it.hasNext()){
	    Point p = it.next();
		p.setX(p.getX()+xx);
		p.sety(p.getY()+yy);
	}
	}
}
