package uvsq.Forme;

public class Main 
{
    public static void main( String[] args )
    {
        Dessin2D D = new Dessin2D();
        Point p1=new Point(0,0);
        Point p2=new Point(3,3);
        Point p3=new Point(5,7);
        Point p4=new Point(6,2);
        Rectangle R=new Rectangle(p1,p2,p3,p4);
        Forme f=new Forme("rectangle",R);
        f.deplacer(10,10);
        Point p6=new Point(8,5);
        Cercle C=new Cercle(p6,6);
        Forme f1=new Forme("cercle",C);
        f1.deplacer(1,1);
        D.ajouter(f);
        D.ajouter(f1);
        D.afficher();
        
        System.out.println("\n\n");
        System.out.println("\n deplacement en Ensemble....\n");
        D.deplacer(2, 3);
        D.afficher();
        System.out.println("\n Upload ....\n");
        D.sauvegarder();
        System.out.println("\n Download ....\n");
        D.charger();
         
         
    }
}
