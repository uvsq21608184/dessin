package uvsq.Forme;
public class Cercle { 
	public Point centre; 
	public int rayon; 
	public Cercle() 
	{ 
		centre = new Point(); 
		rayon =0; 
	}
	
	public Cercle(int x, int y, int r) 
	{ 
		centre = new Point(x,y); 
		rayon = r; 
	}
	
	public Cercle(Point p, int r) 
	{ 
		centre=new Point();
		centre.x=p.getX();
		centre.y=p.getY();
		rayon = r; 
	}
	
    public int getR() {
		return this.rayon;
	}
    
}
