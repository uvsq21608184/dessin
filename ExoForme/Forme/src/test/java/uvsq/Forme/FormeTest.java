package uvsq.Forme;

import static org.junit.Assert.*;

import java.util.ListIterator;

import org.junit.Test;

public class FormeTest {

	@Test
	public void testCercle() {
		Cercle c=new Cercle(1,2,3);
		double r=3;
		assertEquals((Double.valueOf(r)),(Double.valueOf(c.getR())));
		
	}
	
	@Test
	public void testPoint() {
		Point p=new Point(0,0);
		Point p1=new Point(2,2);
		double d=Math.sqrt(8);
		assertEquals((Double.valueOf(d)),(Double.valueOf(p.distance(p1))));
		
	}
	@Test
	public void testDeplacementCercle() {
		Cercle c=new Cercle(1,2,3);
		Forme C=new Forme("cercle",c);
		C.deplacer(2, 3);
		ListIterator<Point> it = C.points.listIterator();
		Point p = it.next();
		double x=3;
		double y=5;
		assertEquals((Double.valueOf(x)),(Double.valueOf(p.getX())));
		assertEquals((Double.valueOf(y)),(Double.valueOf(p.getY())));
		
	}
	@Test
	public void testDeplacementRec() {
		Point p=new Point(1,1);
		Point p1=new Point(1,1);
		Point p2=new Point(1,1);
		Point p3=new Point(1,1);
		Rectangle R=new Rectangle(p1,p2,p3,p);
		Forme C=new Forme("Rectangle",R);
		C.deplacer(2, 3);
		ListIterator<Point> it = C.points.listIterator();
		while(it.hasNext()){
		Point po = it.next();
		double x=3;
		double y=4;
		assertEquals((Double.valueOf(x)),(Double.valueOf(po.getX())));
		assertEquals((Double.valueOf(y)),(Double.valueOf(po.getY())));
		}
	}
	
	
	

}
