package uvsq.Forme;

public class Rectangle {
  public double longueur, largeur;
  public Point HG,HD,BG,BD;
  public Rectangle() {
    this.longueur = 0;
    this.largeur = 0;
    HG=new Point();
    HD=new Point();
    BG=new Point();
    BD=new Point(); 
  }
  public Rectangle(Point p1,Point p2,Point p3,Point p4) {
	    HG=new Point(p1);
	    HD=new Point(p2);
	    BG=new Point(p3);
	    BD=new Point(p4);
	    this.longueur=HG.distance(BG);
	    this.largeur = BG.distance(BD);
	  }
  
  public String toString() {
    return ("le rectangle de longueur " + longueur + " et de largeur " + largeur+HG.Afficher()+HD.Afficher()+BG.Afficher()+BD.Afficher());
  }
}
